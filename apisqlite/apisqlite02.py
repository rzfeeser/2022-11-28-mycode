#!/usr/bin/env python3
""" Author: RZFeeser || Alta3 Research
Gather data returned by various APIs published on OMDB, and cache in a local SQLite DB
"""

import json
import sqlite3
import requests


from flask import Flask
from flask import request

app = Flask(__name__)

# Define the base URL
OMDBURL = "http://www.omdbapi.com/?"



# search for all movies containing string

@app.route("/movielookup")   # GET ... ?mykey=   &searchstring=   &year =    &vtype = 
def movielookup():
    mykey = request.args.get("mykey", None)
    searchstring = request.args.get("searchstring", None)
    year = request.args.get("year", None)
    vtype = request.args.get("vtype", None)
        
    # begin constructing API
    api = f"{OMDBURL}apikey={mykey}&s={searchstring}"
    # user is also searching by year
    if year:
        api = api + f"&y={year}"
    # user is also searching by video type
    if vtype == "movie" or vtype == "series" or vtype == "episode":
        api = api + f"&type={vtype}"

    ## open URL to return 200 response
    resp = requests.get(api)
    ## read the file-like object decode JSON to Python data structure
    return resp.json()


if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224) # runs the application
