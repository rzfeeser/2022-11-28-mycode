#!/usr/bin/python3

# python3 -m pip install requests
import requests

# python3 -m pip install Django
from django.http import JsonResponse    # replaces "import json"

# API to lookup - Django will proxy the request for us
API = "http://api.open-notify.org/astros.json"
    
# https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY
APINASA = "https://api.nasa.gov/planetary/apod?api_key="

APIMTG = "https://api.magicthegathering.io/v1/cards"

def astro(request):    
    res = requests.get(API)
    return JsonResponse(res.json())  # abstraction to return json

def nasa(request):
    apikey = request.GET.get('apikey', 'DEMO_KEY')
    res = requests.get(f"{APINASA}{apikey}")
    return JsonResponse(res.json()) # abstraction to return json

def mtg(request):
    res = requests.get(APIMTG)
    return JsonResponse(res.json())
