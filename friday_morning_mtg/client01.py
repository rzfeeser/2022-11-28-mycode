#!/usr/bin/python3
"""Alta3 Research | rzfeeser@alta3.com
   A simple client to interact with https://api.magicthegathering.io/v1/cards?name=

   Example:
   https://api.magicthegathering.io/v1/cards?name=plains
   https://api.magicthegathering.io/v1/cards?name=swamp"""

# standard library
import argparse

# python3 -m pip install requests
import requests


API = "https://api.magicthegathering.io/v1/cards?"    # name=plains


def main():
    """runtime to interact with api.magicthegathering.io"""

    # we are building a finalized uri to lookup
    uri_to_lookup = API

    # if user supplied this flag
    if args.name:
        uri_to_lookup = uri_to_lookup + "name=" + args.name + "&"

    resp = requests.get(uri_to_lookup)

    if resp.status_code != 200:
        print("Something went wrong with the lookup")
        return

    print(resp.json())


## Define arguments to collect
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # This allows us to pass in a card name to lookup
    parser.add_argument('--name', help='The name of the card type to lookup')

    # now args object has captured any flags that were passed in
    # example: args.name
    args = parser.parse_args()
    main()
